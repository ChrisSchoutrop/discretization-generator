#ifndef INPUT_STRUCT_H
#define INPUT_STRUCT_H
#include <functional>
/*
	+X direction: west -> east
	+Y direction: south-> north
	+Z direction: down -> up
	See Fig. 1 in "Discretization and Parallel Iterative Schemes for Advection-Diffusion-Reaction Problems"
*/
//TODO: Work out dx,dy,dz from the available info (dx is needed to define the flux function)
struct Input1D
{
	//Boundary condition values
	double phi_w;	//West
	double phi_e;	//East

	//Are the boundary conditions Dirichlet? (if they're not, they're Neumann)
	bool phi_w_dirichlet=true;
	bool phi_e_dirichlet=true;

	//Coordinates of the boundaries
	double x_w;
	double x_e;

	//Number of gridpoints in the x-direction
	int N_x;

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux; //flux(u,e,dx)

	//x-Component advection(x):
	std::function<double(double)> u; //u(x)

	//Diffusion (x)
	std::function<double(double)> e; //e(x)

	//Source (x)
	std::function<double(double)> s; //s(x)

	//Linear source(x)
	std::function<double(double)> sl; //sl(x)
};
struct Input2D
{
	//Boundary condition functions
	std::function<double(double)> phi_w; //phi_w(y)
	std::function<double(double)> phi_e; //phi_e(y)
	std::function<double(double)> phi_n; //phi_n(x)
	std::function<double(double)> phi_s; //phi_s(x)

	//Are the boundary conditions Neumann? (if they're not, they're Dirichlet)
	std::function<bool(double)> phi_w_dirichlet{[](double y){ return true;}};
	std::function<bool(double)> phi_e_dirichlet{[](double y){ return true;}};
	std::function<bool(double)> phi_n_dirichlet{[](double x){ return true;}};
	std::function<bool(double)> phi_s_dirichlet{[](double x){ return true;}};

	//Coordinates of the boundaries
	double x_w;
	double x_e;
	double y_s;
	double y_n;

	//Number of gridpoints
	int N_x;
	int N_y;

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux; //flux(u,e,d?)

	//x,y-Components advection(x,y):
	std::function<double(double, double)> u_x; //u_x(x,y)
	std::function<double(double, double)> u_y; //u_y(x,y)

	//Diffusion (x,y)
	std::function<double(double, double)> e; //e(x,y)

	//Constant source (x,y)
	std::function<double(double, double)> s; //s(x,y)

	//Linear source(x,y)
	std::function<double(double, double)> sl; //sl(x,y)
};
struct Input3D
{
	//Boundary condition functions
	std::function<double(double, double)> phi_w; //phi_w(y,z)
	std::function<double(double, double)> phi_e; //phi_e(y,z)
	std::function<double(double, double)> phi_n; //phi_n(x,z)
	std::function<double(double, double)> phi_s; //phi_s(x,z)
	std::function<double(double, double)> phi_d; //phi_d(x,y)
	std::function<double(double, double)> phi_u; //phi_u(x,y)

	//Are the boundary conditions Dirichlet? (if they're not, they're Neumann)
	std::function<bool(double, double)> phi_w_dirichlet{[](double y ,double z){return true;}};
	std::function<bool(double, double)> phi_e_dirichlet{[](double y ,double z){return true;}};
	std::function<bool(double, double)> phi_n_dirichlet{[](double x ,double z){return true;}};
	std::function<bool(double, double)> phi_s_dirichlet{[](double x ,double z){return true;}};
	std::function<bool(double, double)> phi_d_dirichlet{[](double x ,double y){return true;}};
	std::function<bool(double, double)> phi_u_dirichlet{[](double x ,double y){return true;}};

	//Coordinates of the boundaries
	double x_w;
	double x_e;
	double y_s;
	double y_n;
	double z_d;
	double z_u;

	//Number of gridpoints
	int N_x;
	int N_y;
	int N_z;

	//Flux approximation
	std::function<std::array<double, 4>(double, double, double)> flux; //flux(u,e,d?)

	//x,y,z-Components advection(x,y,z):
	std::function<double(double, double, double)> u_x; //u_x(x,y,z)
	std::function<double(double, double, double)> u_y; //u_y(x,y,z)
	std::function<double(double, double, double)> u_z; //u_z(x,y,z)

	//Diffusion (x,y,z)
	std::function<double(double, double, double)> e; //e(x,y,z)

	//Constant source (x,y,z)
	std::function<double(double, double, double)> s; //s(x,y,z)

	//Linear source(x,y,z)
	std::function<double(double, double, double)> sl; //sl(x,y,z)
};

#endif //INPUT_STRUCT_H