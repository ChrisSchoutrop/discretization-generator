#include <iostream>
#include "discretizer.hpp"
void discretizer::discretize(const Input1D& input, SparseMatrixType& A, VectorType& b)
{
	/*
	        Input:
	        Input1D struct
	        Reference to discretization matrix A
	        Reference to rhs vector A

	        This fills A and B such that by solving A*phi=b, we obtain an approximation to:

	        d/dx(u(x) phi(x)-e(x) dphi(x)/dx)=s(x)-sl(x)*phi(x)
	        It is assumed that u and e are piecewise constant between nodes.
	*/
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);

	//Prepare A and b
	//Reserve 3 non-zeros per row (3-point stencil)
	A.resize(input.N_x, input.N_x);
	A.setZero();
	A.reserve(Eigen::VectorXi::Constant(input.N_x, 3));
	b.resize(input.N_x, 1);
	b.setZero();


	//West boundary condition
	if (input.phi_w_dirichlet)
	{
		A.coeffRef(0, 0) += 1;
		b(0) = input.phi_w; //phi(x=0)
	}
	else
	{
		//Estimate Neumann BC using finite difference
		//Note this is first order, but is exact for homogeneous Neumann
		//Implementing ghost points, CD estimate for BC seems like a nightmare to implement at this stage.
		//Q: Would it be possible to just use a higher order forward/backward diff?
		A.coeffRef(0, 0) += -1;
		A.coeffRef(0, 1) += +1;
		b(0) = dx * input.phi_w;
	}

	//East boundary condition
	if (input.phi_e_dirichlet)
	{
		A.coeffRef(input.N_x - 1, input.N_x - 1) += 1;
		b(input.N_x - 1) = input.phi_e; //phi(x=1)
	}
	else
	{
		A.coeffRef(input.N_x - 1, input.N_x - 1) += 1;
		A.coeffRef(input.N_x - 1, input.N_x - 2) += -1;
		b(input.N_x - 1) = dx * input.phi_e;
	}

	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_right;
	std::array<double, 4> F_left;

	//Construct A and b
	for (size_t i = 1; i < input.N_x - 1; ++i)
	{
		//Loop over all non-boundary cells
		double x = input.x_w + i * dx;


		//Evaluate fluxes at the edges of the cell
		F_right = input.flux(input.u(x + 0.5 * dx), input.e(x + 0.5 * dx), dx);
		F_left = input.flux(input.u(x - 0.5 * dx), input.e(x - 0.5 * dx), dx);

		//Loop over all non-boundary cells
		//Conservation law: F_right-F_left=dx*(s-sl*phi)
		//F_left
		A.coeffRef(i, i - 1) -= F_left[alpha];
		A.coeffRef(i, i) -= F_left[beta];
		A.coeffRef(i, i + 1) -= 0;

		//F_right
		A.coeffRef(i, i - 1) += 0;
		A.coeffRef(i, i) += F_right[alpha];
		A.coeffRef(i, i + 1) += F_right[beta];

		//Linear source
		A.coeffRef(i, i) += dx * input.sl(x);

		//F_left
		b(i) -= dx * (-F_left[gamma] * input.s(x - dx) + -F_left[delta] * input.s(x));
		//F_right
		b(i) -= dx * (F_right[gamma] * input.s(x) + F_right[delta] * input.s(x + dx));
		//Source
		b(i) += dx * input.s(x);

	}

	A.makeCompressed();

}
void discretizer::discretize(const Input2D& input, SparseMatrixType& A, VectorType& b)
{
	/*
		Notation: Using the compass stencil, for example a flux going from
		a to b would be Fab.
		Therefore we have the fluxes:
		F_NWN
		F_NNE
		F_WNW
		F_CN
		F_ENE
		F_WC
		F_CE
		F_SWW
		F_SC
		F_SEE
		F_SWS
		F_SSE
		And the nodal points:
		x_NW
		x_N
		x_NE
		x_W
		x_C
		x_E
		x_SW
		x_S
		x_SE

		This fills A and B such that by solving A*phi=b, we obtain an approximation to:

		div(u(x,y) phi(x,y)-e(x,y) grad(phi(x,y))=s(x,y)-sl(x,y)*phi(x,y)
		It is assumed that u and e are piecewise constant between nodes.
	*/
	//Grid spacing
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);

	//Prepare A and b
	A.resize(input.N_x * input.N_y, input.N_x * input.N_y);
	A.setZero();

	//5 points for non-crossflux schemes
	A.reserve(Eigen::VectorXi::Constant(input.N_x * input.N_y, 5));

	b.resize(input.N_x * input.N_y, 1);
	b.setZero();

	//Define variables needed to construct the discretization
	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};
	std::array<double, 4> F_CN;
	std::array<double, 4> F_WC;
	std::array<double, 4> F_CE;
	std::array<double, 4> F_SC;

	//Indices of neighbouring cells
	int ix_N;
	int ix_W;
	int ix_C;
	int ix_E;
	int ix_S;
	double x, y; //Position of the nodal point

	for (int i = 0; i < input.N_x; i++)
	{


		for (int j = 0; j < input.N_y; j++)
		{
			//Indices of neighbouring cells

			ix_N =  (i + 0) + input.N_x * (j + 1);
			ix_W =  (i - 1) + input.N_x * (j + 0);
			ix_C =  (i + 0) + input.N_x * (j + 0);
			ix_E =  (i + 1) + input.N_x * (j + 0);
			ix_S =  (i + 0) + input.N_x * (j - 1);

			//Position of the nodal point
			x = input.x_w + i * dx;
			y = input.y_s + j * dy;

			//Work out the boundary conditions
			if (i == 0)
			{
				//West boundary
				if (input.phi_w_dirichlet(y))
				{
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_w(y);
				}
				else
				{
					A.coeffRef(ix_C, ix_E) += 1;
					A.coeffRef(ix_C, ix_C) -= 1;
					b(ix_C) = dx * input.phi_w(y);
				}

			}
			else if (i == input.N_x - 1)
			{
				//East boundary
				if (input.phi_e_dirichlet(y))
				{
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_e(y);
				}
				else
				{
					A.coeffRef(ix_C, ix_C) += 1;
					A.coeffRef(ix_C, ix_W) -= 1;
					b(ix_C) = dx * input.phi_e(y);
				}

			}
			else if (j == 0)
			{
				//South boundary
				if (input.phi_s_dirichlet(x))
				{
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_s(x);
				}
				else
				{
					A.coeffRef(ix_C, ix_N) += 1;
					A.coeffRef(ix_C, ix_C) -= 1;
					b(ix_C) = dy * input.phi_s(x);
				}

			}
			else if (j == input.N_y - 1)
			{
				//North boundary
				if (input.phi_n_dirichlet(x))
				{
					A.coeffRef(ix_C, ix_C) += 1;
					b(ix_C) = input.phi_n(x);
				}
				else
				{
					A.coeffRef(ix_C, ix_C) += 1;
					A.coeffRef(ix_C, ix_S) -= 1;
					b(ix_C) = dy * input.phi_n(x);
				}

			}
			else
			{
				//NOTE: For efficient assembly it would probably be faster to first compute all fluxes in the entire domain
				//Instead of considering each cell individually, currently many flux calculations are not recycled.
				F_CN  = input.flux(input.u_y(x, y + dy / 2), input.e(x, y + dy / 2), dy);
				F_WC  = input.flux(input.u_x(x - dx / 2, y), input.e(x - dx / 2, y), dx);
				F_CE  = input.flux(input.u_x(x + dx / 2, y), input.e(x + dx / 2, y), dx);
				F_SC  = input.flux(input.u_y(x, y - dy / 2), input.e(x, y - dy / 2), dy);

				//Not at a boundary
				//CF Scheme without the effects of cross flux:

				//North flux
				A.coeffRef(ix_C, ix_S) += 0;
				A.coeffRef(ix_C, ix_C) += dx * F_CN[alpha];
				A.coeffRef(ix_C, ix_N) += dx * F_CN[beta];
				b(ix_C) -= dy * dx * (F_CN[gamma] * input.s(x, y) + F_CN[delta] * input.s(x,
							y + dy));
				//East flux
				A.coeffRef(ix_C, ix_W) += 0;
				A.coeffRef(ix_C, ix_C) += dy * F_CE[alpha];
				A.coeffRef(ix_C, ix_E) += dy * F_CE[beta];
				b(ix_C) -= dx * dy * (F_CE[gamma]  * input.s(x,
							y) + F_CE[delta] * input.s(x + dx, y));
				//South flux
				A.coeffRef(ix_C, ix_S) -= dx * F_SC[alpha];
				A.coeffRef(ix_C, ix_C) -= dx * F_SC[beta];
				A.coeffRef(ix_C, ix_N) -= 0;
				b(ix_C) += dy * dx * (F_SC[gamma] * input.s(x,
							y - dy) + F_SC[delta] * input.s(x, y));
				//West flux
				A.coeffRef(ix_C, ix_W) -= dy * F_WC[alpha];
				A.coeffRef(ix_C, ix_C) -= dy * F_WC[beta];
				A.coeffRef(ix_C, ix_E) -= 0;
				b(ix_C) += dx * dy * (F_WC[gamma] * input.s(x - dx,
							y) + F_WC[delta] * input.s(x, y));
				//Linear source
				A.coeffRef(ix_C, ix_C) += dx * dy * input.sl(x, y);
				//Other source
				b(ix_C) += dx * dy * input.s(x, y);

			}
		}

	}

	A.makeCompressed();

}
void discretizer::discretize(const Input3D& input, SparseMatrixType& A, VectorType& b)
{
	/*
		TODO:
		-crossflux
		-Set expected number of equations/row
		-Check result
		Input:
		Input3D struct
		Reference to discretization matrix A
		Reference to rhs vector A

		This fills A and B such that by solving A*phi=b, we obtain an approximation to:

		div(u(x,y,z) phi(x,y,z)-e(x,y,z) grad(phi(x,y,z))=s(x,y,z)-sl(x,y,z)*phi(x,y,z)
		It is assumed that u and e are piecewise constant between nodes.
	*/
	const double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	const double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	const double dz = (input.z_u - input.z_d) / (input.N_z - 1);

	//Prepare A and b
	A.resize(input.N_x * input.N_y * input.N_z, input.N_x * input.N_y * input.N_z);
	A.setZero();

	//non-crossflux schemes use a 7 point stencil
	A.reserve(Eigen::VectorXi::Constant(input.N_x * input.N_y * input.N_z, 7));

	b.resize(input.N_x * input.N_y * input.N_z, 1);
	b.setZero();

	//Define variables needed to construct the discretization
	//Allocate arrays for the flux coefficients
	enum coefficient {alpha, beta, gamma, delta};

	//Allocate arrays for the flux coefficients
	//Each vertex connecting two edges nodes in the compass stencil
	//Fluxes oriented in +x+y+z direction
	std::array<double, 4>
	F_CN,   F_SC,  F_WC, F_CE, F_DCC, F_CUC;

	//Indices of neighbouring cells
	//Each edge in the compass stencil
	int ix_UC, ix_N, ix_W, ix_C, ix_E, ix_S, ix_DC ;
	double x, y, z; //Position of the nodal point

	for (int i = 0; i < input.N_x; i++)
	{
		for (int j = 0; j < input.N_y; j++)
		{
			for (int k = 0; k < input.N_z; k++)
			{
				//Indices of neighbouring cells

				ix_UC  = (i + 0) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 1);
				ix_DC  = (i + 0) + input.N_x * (j + 0) + input.N_x * input.N_y * (k - 1);
				ix_C   = (i + 0) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 0);
				ix_N   = (i + 0) + input.N_x * (j + 1) + input.N_x * input.N_y * (k + 0);
				ix_E   = (i + 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 0);
				ix_S   = (i + 0) + input.N_x * (j - 1) + input.N_x * input.N_y * (k + 0);
				ix_W   = (i - 1) + input.N_x * (j + 0) + input.N_x * input.N_y * (k + 0);

				//Position of the nodal point
				x = input.x_w + i * dx;
				y = input.y_s + j * dy;
				z = input.z_d + k * dz;

				//Work out the Dirichlet boundary conditions
				if (i == 0)
				{
					//West boundary
					if (input.phi_w_dirichlet(y, z))
					{
						A.coeffRef(ix_C, ix_C) += 1;
						b(ix_C) = input.phi_w(y, z);
					}
					else
					{
						A.coeffRef(ix_C, ix_E) += 1;
						A.coeffRef(ix_C, ix_C) -= 1;
						b(ix_C) = dx * input.phi_w(y, z);
					}
				}
				else if (i == input.N_x - 1)
				{
					//East boundary
					if (input.phi_e_dirichlet(y, z))
					{
						A.coeffRef(ix_C, ix_C) += 1;
						b(ix_C) = input.phi_e(y, z);
					}
					else
					{
						A.coeffRef(ix_C, ix_C) += 1;
						A.coeffRef(ix_C, ix_W) -= 1;
						b(ix_C) = dx * input.phi_e(y, z);
					}

				}
				else if (j == 0)
				{
					//South boundary
					if (input.phi_s_dirichlet(x, z))
					{
						A.coeffRef(ix_C, ix_C) += 1;
						b(ix_C) = input.phi_s(x, z);
					}
					else
					{
						A.coeffRef(ix_C, ix_N) += 1;
						A.coeffRef(ix_C, ix_C) -= 1;
						b(ix_C) = dy * input.phi_s(x, z);
					}

				}
				else if (j == input.N_y - 1)
				{
					//North boundary
					if (input.phi_n_dirichlet(x, z))
					{
						A.coeffRef(ix_C, ix_C) += 1;
						b(ix_C) = input.phi_n(x, z);
					}
					else
					{
						A.coeffRef(ix_C, ix_C) += 1;
						A.coeffRef(ix_C, ix_S) -= 1;
						b(ix_C) = dy * input.phi_n(x, z);
					}

				}
				else if (k == 0)
				{
					//Down boundary
					if (input.phi_d_dirichlet(x, y))
					{
						A.coeffRef(ix_C, ix_C) += 1;
						b(ix_C) = input.phi_d(x, y);
					}
					else
					{
						A.coeffRef(ix_C, ix_UC) += 1;
						A.coeffRef(ix_C, ix_C) -= 1;
						b(ix_C) = dz * input.phi_d(x, y);
					}

				}
				else if (k == input.N_z - 1)
				{
					//Up boundary
					if (input.phi_u_dirichlet(x, y))
					{
						A.coeffRef(ix_C, ix_C) += 1;
						b(ix_C) = input.phi_u(x, y);
					}
					else
					{
						A.coeffRef(ix_C, ix_C) += 1;
						A.coeffRef(ix_C, ix_DC) -= 1;
						b(ix_C) = dz * input.phi_n(x, y);
					}

				}
				else
				{

					F_CN  = input.flux(input.u_y(x + 00, y + dy / 2, z + 00 ), input.e(x + 00, y + dy / 2, z + 00 ),
							dy);
					F_SC  = input.flux(input.u_y(x + 00, y - dy / 2, z + 00 ), input.e(x + 00, y + dy / 2, z + 00 ),
							dy);
					F_WC  = input.flux(input.u_x(x - dx / 2, y + 00, z + 00 ), input.e(x - dx / 2, y + 00, z + 00 ),
							dx);
					F_CE  = input.flux(input.u_x(x + dx / 2, y + 00, z + 00 ), input.e(x + dx / 2, y + 00, z + 00 ),
							dx);
					F_DCC = input.flux(input.u_z(x + 00, y + 00, z - dz / 2), input.e(x + 00, y + 00, z - dz / 2), dz);
					F_CUC = input.flux(input.u_z(x + 00, y + 00, z + dz / 2), input.e(x + 00, y + 00, z + dz / 2), dz);

					//Non-cross fluxes:
					//North flux
					A.coeffRef(ix_C, ix_S) += 0;
					A.coeffRef(ix_C, ix_C) += dx * dz * F_CN[alpha];
					A.coeffRef(ix_C, ix_N) += dx * dz * F_CN[beta];
					b(ix_C) -= dy * dx * dz * (F_CN[gamma] * input.s(x, y,
								z) + F_CN[delta] * input.s(x, y + dy, z));
					//East flux
					A.coeffRef(ix_C, ix_W) += 0;
					A.coeffRef(ix_C, ix_C) += dy * dz * F_CE[alpha];
					A.coeffRef(ix_C, ix_E) += dy * dz * F_CE[beta];
					b(ix_C) -= dx * dy * dz * (F_CE[gamma] * input.s(x, y,
								z) + F_CE[delta] * input.s(x + dx, y, z));
					//South flux
					A.coeffRef(ix_C, ix_S) -= dx * dz * F_SC[alpha];
					A.coeffRef(ix_C, ix_C) -= dx * dz * F_SC[beta];
					A.coeffRef(ix_C, ix_N) -= 0;
					b(ix_C) += dy * dx * dz * (F_SC[gamma] * input.s(x, y - dy,
								z) + F_SC[delta] * input.s(x, y, z));
					//West flux
					A.coeffRef(ix_C, ix_W) -= dy * dz * F_WC[alpha];
					A.coeffRef(ix_C, ix_C) -= dy * dz * F_WC[beta];
					A.coeffRef(ix_C, ix_E) -= 0;
					b(ix_C) += dx * dy * dz * (F_WC[gamma] * input.s(x - dx, y,
								z) + F_WC[delta] * input.s(x, y, z));
					//Up flux
					A.coeffRef(ix_C, ix_DC) += 0;
					A.coeffRef(ix_C, ix_C) += dx * dy * F_CUC[alpha];
					A.coeffRef(ix_C, ix_UC) += dx * dy * F_CUC[beta];
					b(ix_C) -= dz * dx * dy * (F_CUC[gamma] * input.s(x, y,
								z) + F_CUC[delta] * input.s(x, y, z + dz));
					//Down flux
					A.coeffRef(ix_C, ix_DC) -= dx * dy * F_DCC[alpha];
					A.coeffRef(ix_C, ix_C) -= dx * dy * F_DCC[beta];
					A.coeffRef(ix_C, ix_UC) -= 0;
					b(ix_C) += dz * dx * dy * (F_DCC[gamma] * input.s(x, y,
								z - dz) + F_DCC[delta] * input.s(x, y, z));
					//Linear source
					A.coeffRef(ix_C, ix_C) += dx * dy * dz * input.sl(x, y, z);
					//Other source
					b(ix_C) += dx * dy * dz * input.s(x, y, z);

				}
			}
		}

	}

	A.makeCompressed();
}