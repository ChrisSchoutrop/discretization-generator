#include <iostream>
#include "flux.hpp"
#include "mf.hpp"
#include "input_struct.hpp"
#include "discretizer.hpp"
#include "eigen3/Eigen/Sparse"
#include "eigen3/Eigen/Dense"
typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrixType;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorType;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> DenseMatrixTypeCol;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> DenseMatrixTypeRow;


void generate_1d(SparseMatrixType& A, VectorType& rhs)
{
	//Set up the input data for the discretization
	Input1D input;

	//Size of the domain
	input.x_w = 0.0;
	input.x_e = 1;

	//Number of grid points
	input.N_x = 25;

	//Parameters
	double u = 2.7182818284;
	double e = 1.41421;
	double s = 1.73205;
	double sl = 2.2361;

	//Boundary values
	input.phi_w = 1.0;
	input.phi_e = 0.0;

	//Grid spacing
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);

	//Advection velocity
	input.u = [u](double x)
	{
		return u;
	};

	//Diffusion coefficient
	input.e = [e](double x)
	{
		return e;
	};

	//Source term
	input.s = [s](double x)
	{
		return s;
	};
	input.sl = [sl](double x)
	{
		return sl;
	};

	//Flux used in the discretization scheme
	input.flux = [](double u, double e, double dx)
	{
		return flux::CD(u, e, dx);
	};

	//Generate the discretization matrix A and corresponding
	//right hand side rhs
	discretizer::discretize(input, A, rhs);
}

void generate_2d(SparseMatrixType& A, VectorType& rhs)
{
	//Set up the input data for the discretization
	Input2D input;

	//Size of the domain
	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;

	//Number of grid points
	input.N_x = 25;
	input.N_y = 25;

	//Parameters
	double u = 1;
	double e = 1;
	double s = 1;
	double sl = 0;

	//Boundary value functions
	input.phi_w = [](double y)
	{
		return 1.0;
	};
	input.phi_e = [](double y)
	{
		return 0.0;
	};
	input.phi_n = [](double x)
	{
		return 1.0;
	};
	input.phi_s = [](double x)
	{
		return 0.0;
	};

	//Grid spacing
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);

	//Advection velocities
	input.u_x = [u](double x, double y)
	{
		return u;
	};
	input.u_y = [u](double x, double y)
	{
		return u;
	};

	//Diffusion coefficient
	input.e = [e](double x, double y)
	{
		return e;
	};

	//Source term
	input.s = [s](double x, double y)
	{
		return s;
	};
	input.sl = [sl](double x, double y)
	{
		return sl;
	};

	//Flux used in the discretization scheme
	input.flux = [](double u, double e, double dx)
	{
		return flux::CD(u, e, dx);
	};

	//Generate the discretization matrix A and corresponding
	//right hand side rhs
	discretizer::discretize(input, A, rhs);
}

void generate_3d(SparseMatrixType& A, VectorType& rhs)
{
	//Set up the input data for the discretization
	Input3D input;

	//Size of the domain
	input.x_w = 0;
	input.x_e = 1;
	input.y_s = 0;
	input.y_n = 1;
	input.z_d = 0;
	input.z_u = 1;

	//Number of grid points
	input.N_x = 25;
	input.N_y = 25;
	input.N_z = 25;

	//Parameters
	double u = 2.7182818284;
	double e = 1.41421;
	double s = 1.73205;
	double sl = 2.2361;

	//Boundary value functions
	input.phi_w = [](double y, double z)
	{
		return 1.0;
	};
	input.phi_e = [](double y, double z)
	{
		return 0.0;
	};
	input.phi_n = [](double x, double z)
	{
		return 0.0;
	};
	input.phi_s = [](double x, double z)
	{
		return 0.0;
	};
	input.phi_d = [](double x, double y)
	{
		return 0.0;
	};
	input.phi_u = [](double x, double y)
	{
		return 1.0;
	};

	//Grid spacing
	double dx = (input.x_e - input.x_w) / (input.N_x - 1);
	double dy = (input.y_n - input.y_s) / (input.N_y - 1);
	double dz = (input.z_u - input.z_d) / (input.N_z - 1);

	//Advection velocities
	input.u_x = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_y = [u](double x, double y, double z)
	{
		return u;
	};
	input.u_z = [u](double x, double y, double z)
	{
		return u;
	};

	//Diffusion coefficient
	input.e = [e](double x, double y, double z)
	{
		return e;
	};

	//Source term
	input.s = [s](double x, double y, double z)
	{
		return s;
	};
	input.sl = [sl](double x, double y, double z)
	{
		return sl;
	};

	//Flux used in the discretization scheme
	input.flux = [](double u, double e, double dx)
	{
		return flux::CD(u, e, dx);
	};

	//Generate the discretization matrix A and corresponding
	//right hand side rhs
	discretizer::discretize(input, A, rhs);
}

int main()
{
	SparseMatrixType A;
	VectorType rhs;

	generate_1d(A, rhs);
	std::cout << A.nonZeros() << std::endl;
	std::cout << rhs.size() << std::endl;

	generate_2d(A, rhs);
	std::cout << A.nonZeros() << std::endl;
	std::cout << rhs.size() << std::endl;

	generate_3d(A, rhs);
	std::cout << A.nonZeros() << std::endl;
	std::cout << rhs.size() << std::endl;

}